package com.ioasys.ivan.apiConnection;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ioasys.ivan.Home;
import com.ioasys.ivan.Response.EmpresaDetalhes;
import com.ioasys.ivan.Response.Investidor;
import com.ioasys.ivan.adapters.EmpresasCustomAdapter;
import com.ioasys.ivan.empresas.R;
import com.ioasys.ivan.Response.EmpresaObj;
import com.ioasys.ivan.Response.ListaEmpresas;
import com.ioasys.ivan.Response.User;

import java.io.Serializable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ivanz on 16/07/2017.
 */

public class Connection {
    public static final String urlBase = "http://54.94.179.135:8090/api/v1/";
    public static final String LOGIN_URL = "users/auth/sign_in/";
    public static final String ENTERPRISE_INDEX_URL = "enterprises";
    public static final String SHOW_URL = "enterprises/{id}";
    public static final String ACCESS_TOKEN = "access-token";
    public static final String CLIENT = "client";
    public static final String UID = "uid";

    static String access_token;
    static String client;
    static String uid;

    private static RestApiConnection getRetrofit(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(urlBase)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(RestApiConnection.class);
    }

    private static void trocaBotaoLogin(int estado, View v){
        if(estado == 1){
            v.setEnabled(true);
            v.setBackgroundColor(0xFF57BBBC);
        }
        else if(estado == 2) {
            v.setEnabled(false);
            v.setBackgroundColor(0xFF617C7B);
        }
    }

    public static void logar (final LoginObj loginObj, final Context context){
        final View botaoLogin = ((Activity)context).findViewById(R.id.btn_entrar);
        trocaBotaoLogin(2, botaoLogin);
        Call<User> call = getRetrofit().login(loginObj);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                trocaBotaoLogin(1, botaoLogin);
                if (response.isSuccessful()) {
                    Toast.makeText(context, "Conectado", Toast.LENGTH_SHORT).show();
                    access_token = response.headers().get(ACCESS_TOKEN);
                    client = response.headers().get(CLIENT);
                    uid = response.headers().get(UID);
                    Intent intent = new Intent(context, Home.class);
                    intent.putExtra("Usuario", (new Gson().toJson(response.body().getInvestidor(), Investidor.class)));
                    context.startActivity(intent);
                }
                else
                    Toast.makeText(context, "Credenciais incorretas", Toast.LENGTH_SHORT).show();
            }
            public void onFailure(Call<User> call, Throwable t) {
                trocaBotaoLogin(1, botaoLogin);
                Toast.makeText(context, "Sem Conexão", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void buscarPorNome (String nome, final Context context){
        Call<ListaEmpresas> call = getRetrofit().buscarPorNome("1",nome, access_token, client, uid);
        call.enqueue(new Callback<ListaEmpresas>() {
            @Override
            public void onResponse(Call<ListaEmpresas> call, Response<ListaEmpresas> response) {
                if (response.isSuccessful()) {
                    ListView listaEmpresas = ((ListView) ((Activity) context).findViewById(R.id.lvEmpresas));
                    ArrayAdapter<EmpresaObj> adapterEmpresas = new EmpresasCustomAdapter(
                            context,response.body().getEmpresaObjs());
                    listaEmpresas.setAdapter(adapterEmpresas);
                }
                else
                    Toast.makeText(context, "Credenciais incorretas", Toast.LENGTH_SHORT).show();
            }
            public void onFailure(Call<ListaEmpresas> call, Throwable t) {
                Toast.makeText(context, "Sem Conexão", Toast.LENGTH_SHORT).show();
                Log.d("err", t.getMessage());
            }
        });
    }

    public static void buscarDetalhesEmpresa (int id, final Context context){
        Call<EmpresaDetalhes> call = getRetrofit().buscarDetalhesEmpresa(id, access_token, client, uid);
        call.enqueue(new Callback<EmpresaDetalhes>() {
            @Override
            public void onResponse(Call<EmpresaDetalhes> call, Response<EmpresaDetalhes> response) {
                if (response.isSuccessful()) {
                    ((TextView)((Activity)context).findViewById(R.id.tvEmpresaEscolhida)).
                            setText(response.body().getEnterprise().getEnterprise_name());
                    ((TextView)((Activity)context).findViewById(R.id.tvDescricaoEmpresa)).
                            setText(Html.fromHtml(response.body().getEnterprise().getDetalhes()));
                }
                else
                    Toast.makeText(context, "Credenciais incorretas", Toast.LENGTH_SHORT).show();
            }
            public void onFailure(Call<EmpresaDetalhes> call, Throwable t) {
                Toast.makeText(context, "Sem Conexão", Toast.LENGTH_SHORT).show();
                Log.d("err", t.getMessage());
            }
        });
    }
}
