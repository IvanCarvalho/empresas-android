package com.ioasys.ivan.Response;

/**
 * Created by ivanz on 19/07/2017.
 */

public class Investidor {
    private String investor_name, email, city, country;
    private float balance, portifolio_value;
    private Boolean first_acess, super_angel;
    private Portifolio portifolio;

    @Override
    public String toString(){
        return
            "<b>Email</b><br/> "+email+"<br/><br/>"+
            "<b>Nome</b><br/> "+investor_name+"<br/><br/>"+
            "<b>Cidade</b><br/> "+city+"<br/><br/>"+
            "<b>Pais</b><br/> "+country+"<br/><br/>"+
            "<b>Saldo</b><br/> "+balance+"<br/><br/>"+
            "<b>Valor Portifólio</b><br/> "+portifolio_value+"<br/><br/>";
    }
}
