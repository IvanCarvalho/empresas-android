package com.ioasys.ivan.Response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ivanz on 26/07/2017.
 */

public class TipoEmpresa {
    int id;
    @SerializedName("enterprise_type_name")
    String tipo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }
}
