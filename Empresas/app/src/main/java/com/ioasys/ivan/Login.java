package com.ioasys.ivan;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ioasys.ivan.Util.UtilFont;
import com.ioasys.ivan.empresas.R;
import com.ioasys.ivan.apiConnection.Connection;
import com.ioasys.ivan.apiConnection.LoginObj;

public class Login extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        ((TextView)findViewById(R.id.textView2)).setTypeface(UtilFont.getRobotoBold(this));
        ((TextView)findViewById(R.id.textView)).setTypeface(UtilFont.getRobotoRegular(this));
        ((Button)findViewById(R.id.btn_entrar)).setTypeface(UtilFont.getGilsans(this));

        Button entrar = (Button) findViewById(R.id.btn_entrar);
        final Context atual = this;
        entrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Connection.logar(new LoginObj(
                        ((EditText) findViewById(R.id.email)).getText().toString(),
                        ((EditText) findViewById(R.id.senha)).getText().toString()
                ), atual);
            }
        });
    }
}
