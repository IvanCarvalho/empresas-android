package com.ioasys.ivan.Util;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by ivanz on 26/07/2017.
 */

public class UtilFont {
    public static Typeface getRobotoBold(Context context){
        return Typeface.createFromAsset(context.getAssets(),"fonts/Roboto-Bold.ttf");
    }

    public static Typeface getRobotoRegular(Context context){
        return Typeface.createFromAsset(context.getAssets(),"fonts/Roboto-Regular.ttf");
    }

    public static Typeface getGilsans(Context context){
        return Typeface.createFromAsset(context.getAssets(),"fonts/GillSans-SemiBold.ttf");
    }
}
