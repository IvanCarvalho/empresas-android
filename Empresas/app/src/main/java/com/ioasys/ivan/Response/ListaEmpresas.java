package com.ioasys.ivan.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ivanz on 23/07/2017.
 */

public class ListaEmpresas {
    @SerializedName("enterprises")
    List<EmpresaObj> empresaObjs;

    public List<EmpresaObj> getEmpresaObjs() {
        return empresaObjs;
    }
}
