package com.ioasys.ivan.Response;

public class EmpresaObj {
    private int id;
    private String email_enterprise;
    private String facebook;
    private String twitter;
    private String linkedin;
    private String phone;
    private String own_enterprise;
    private String enterprise_name;
    private String photo;
    private String description;
    private String city;
    private String country;
    private Float value;
    private Float share_price;
    private Float shares;
    private TipoEmpresa enterprise_type;

    public String getDetalhes(){
        return
            "<b>Nome</b><br/> "+enterprise_name+"<br/><br/>"+
            "<b>Descrição</b><br/> "+description+"<br/><br/>"+
            "<b>Cidade</b><br/> "+city+"<br/><br/>"+
            "<b>Pais</b><br/> "+country+"<br/><br/>"+
            "<b>Telefone</b><br/> "+phone+"<br/><br/>"+
            "<b>Email</b><br/> "+email_enterprise+"<br/><br/>"+
            "<b>Valor</b><br/> "+value+"<br/><br/>"+
            "<b>Valor das ações</b><br/> "+share_price+"<br/><br/>"+
            "<b>Ações</b><br/> "+shares+"<br/><br/>"+
            "<b>Facebook</b><br/> "+facebook+"<br/><br/>"+
            "<b>Twitter</b><br/> "+twitter+"<br/><br/>"+
            "<b>Linkedin</b><br/> "+linkedin+"<br/><br/>"+
            "<b>Empresa Própria</b><br/> "+(Boolean.getBoolean(own_enterprise) ? "Sim" : "Não")+"<br/><br/>";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEnterprise_name() {
        return enterprise_name;
    }

    public String getCountry() {
        return country;
    }

    public TipoEmpresa getEnterprise_type() {
        return enterprise_type;
    }
}
