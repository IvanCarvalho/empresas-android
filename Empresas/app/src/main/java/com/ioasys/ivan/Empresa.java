package com.ioasys.ivan;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ioasys.ivan.empresas.R;
import com.ioasys.ivan.apiConnection.Connection;

public class Empresa extends AppCompatActivity {
    TextView descricaoEmpresa;
    ImageView botaoVoltar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.empresa);
        int id = getIntent().getIntExtra("empresaID", -1);
        if(id == -1){
            Toast.makeText(this,"Erro interno", Toast.LENGTH_SHORT).show();
            finish();
        }

        botaoVoltar = (ImageView)findViewById(R.id.ivBack);
        descricaoEmpresa = (TextView)findViewById(R.id.tvDescricaoEmpresa);
        botaoVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Connection.buscarDetalhesEmpresa(id, this);
    }
}
