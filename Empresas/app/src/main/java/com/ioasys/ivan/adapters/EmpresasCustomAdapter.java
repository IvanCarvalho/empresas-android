package com.ioasys.ivan.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ioasys.ivan.Empresa;
import com.ioasys.ivan.Util.UtilFont;
import com.ioasys.ivan.empresas.R;
import com.ioasys.ivan.Response.EmpresaObj;

import java.util.List;

/**
 * Created by ivanz on 25/07/2017.
 */

public class EmpresasCustomAdapter extends ArrayAdapter<EmpresaObj> {

    Context context;

    public EmpresasCustomAdapter(Context context, List<EmpresaObj> lista) {
        super(context, R.layout.template_empresas, lista);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = inflater.inflate(R.layout.template_empresas, parent, false);

        TextView nomeEmpresa = (TextView)customView.findViewById(R.id.tvEmpresaNome);
        TextView negocioEmpresa = (TextView)customView.findViewById(R.id.tvEmpresaNegocio);
        TextView paisEmpresa = (TextView)customView.findViewById(R.id.tvEmpresaPais);

        final EmpresaObj empresa = getItem(position);

        nomeEmpresa.setText(empresa.getEnterprise_name());
        nomeEmpresa.setTypeface(UtilFont.getRobotoBold(context));
        negocioEmpresa.setText(empresa.getEnterprise_type().getTipo());
        negocioEmpresa.setTypeface(UtilFont.getRobotoRegular(context));
        paisEmpresa.setText(empresa.getCountry());
        paisEmpresa.setTypeface(UtilFont.getRobotoRegular(context));

        customView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Empresa.class);
                intent.putExtra("empresaID", empresa.getId());
                context.startActivity(intent);
            }
        });
        return customView;
    }
}
