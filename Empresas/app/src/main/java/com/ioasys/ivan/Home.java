package com.ioasys.ivan;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ioasys.ivan.Response.Credenciais;
import com.ioasys.ivan.Response.Investidor;
import com.ioasys.ivan.Response.User;
import com.ioasys.ivan.empresas.R;
import com.ioasys.ivan.apiConnection.Connection;

public class Home extends AppCompatActivity {
    RelativeLayout menuHome;
    LinearLayout menuBusca;
    ListView listaEmpresas;
    ImageView fechaBusca;
    EditText campoBusca;
    int estadoMenu = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        final Context context = this;

        menuBusca = (LinearLayout)findViewById(R.id.menuBusca);
        menuHome = (RelativeLayout) findViewById(R.id.menuHome);
        fechaBusca = (ImageView)findViewById(R.id.tvcloseSearch);
        campoBusca = (EditText)findViewById(R.id.edSearch);
        listaEmpresas = (ListView) findViewById(R.id.lvEmpresas);
        trocaMenu(2);

        Investidor usuario = new Gson().fromJson(getIntent().getStringExtra("Usuario"), Investidor.class);
        ((TextView)findViewById(R.id.tvHome)).setText(Html.fromHtml(usuario.toString()+"<br/>Clique na busca para iniciar."));

        ImageView pesquisar = (ImageView) findViewById(R.id.search_btn);

        pesquisar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Connection.buscarPorNome("a", context);
                trocaMenu(1);
            }
        });

        fechaBusca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trocaMenu(2);
            }
        });

        campoBusca.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Connection.buscarPorNome(s.toString(), context);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onBackPressed()
    {
        if(estadoMenu == 1){
            estadoMenu = 2;
            trocaMenu(2);
        }
        else if(estadoMenu == 2)
            finish();
    }

    private void trocaMenu(int menu){
        // Buscar
        if(menu == 1){
            estadoMenu = 1;
            menuHome.setVisibility(View.INVISIBLE);
            (findViewById(R.id.tvHome)).setVisibility(View.INVISIBLE);
            menuBusca.setVisibility(View.VISIBLE);
            (findViewById(R.id.lvEmpresas)).setVisibility(View.VISIBLE);
        }
        // Home
        else if(menu == 2){
            estadoMenu = 2;
            menuHome.setVisibility(View.VISIBLE);
            (findViewById(R.id.tvHome)).setVisibility(View.VISIBLE);
            menuBusca.setVisibility(View.INVISIBLE);
            (findViewById(R.id.lvEmpresas)).setVisibility(View.INVISIBLE);
        }
    }
}
