package com.ioasys.ivan.apiConnection;

import com.ioasys.ivan.Response.EmpresaDetalhes;
import com.ioasys.ivan.Response.ListaEmpresas;
import com.ioasys.ivan.Response.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by ivanz on 19/07/2017.
 */
public interface RestApiConnection {
    @POST(Connection.LOGIN_URL)
    Call<User> login(@Body LoginObj loginObj);

    @GET(Connection.ENTERPRISE_INDEX_URL)
    Call<ListaEmpresas> buscarPorNome(
            @Query("enterprise_types") String tipo,
            @Query("name") String name,
            @Header("access-token") String token,
            @Header("client") String client,
            @Header("uid") String uid
    );

    @GET(Connection.SHOW_URL)
    Call<EmpresaDetalhes> buscarDetalhesEmpresa(
            @Path("id") int id,
            @Header("access-token") String token,
            @Header("client") String client,
            @Header("uid") String uid
    );
}
