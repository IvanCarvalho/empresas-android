package com.ioasys.ivan.Response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ivanz on 19/07/2017.
 */

public class User {
    @SerializedName("investor")
    Investidor investidor;
    String enterprise;
    String success;


    public Investidor getInvestidor(){
        return investidor;
    }
    @Override
    public String toString(){
        return investidor.toString();
    }
}
